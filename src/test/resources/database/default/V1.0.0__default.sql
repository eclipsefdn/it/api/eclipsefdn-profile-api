CREATE TABLE `gerrit_review_count` (
  `uid` int NOT NULL DEFAULT 0,
  review_count int NOT NULL DEFAULT 0,
  report_date int NOT NULL DEFAULT 0,
  PRIMARY KEY (`uid`)
);
-- These records belong to mock user "firstlast"
INSERT INTO gerrit_review_count(`uid`, `review_count`, `report_date`)
  VALUES (42, 5000, 123456789);

CREATE TABLE `user_delete_request` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` int NOT NULL DEFAULT 0,
  `name` varchar(60) NOT NULL,
  `mail` varchar(254) NOT NULL,
  `host` varchar(2048) NOT NULL,
  `status` int NOT NULL DEFAULT 0,
  `created` int NOT NULL DEFAULT 0,
  `changed` int NOT NULL DEFAULT 0,
  PRIMARY KEY(`id`)
);

INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'accounts.eclipse.org', 0, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'projects.eclipse.org', 0, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'marketplace.eclipse.org', 0, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'blogs.eclipse.org', 0, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'eclipse.org/downloads/packages', 0, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (666, 'username', 'email@fake.com', 'newsroom.eclipse.org', 0, 134567891, 134567891);

-- Mock data below is used to validate "final-stage" functionality. 2 records should be returned
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (11, 'testytesterson', 'testy@fake.com', 'accounts.eclipse.org', 0, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (11, 'testytesterson', 'testy@fake.com', 'projects.eclipse.org', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (11, 'testytesterson', 'testy@fake.com', 'marketplace.eclipse.org', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (11, 'testytesterson', 'testy@fake.com', 'blogs.eclipse.org', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (11, 'testytesterson', 'testy@fake.com', 'eclipse.org/downloads/packages', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (11, 'testytesterson', 'testy@fake.com', 'newsroom.eclipse.org', 1, 134567891, 134567891);

INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (22, 'johnsmith', 'smitherines@fake.com', 'accounts.eclipse.org', 0, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (22, 'johnsmith', 'smitherines@fake.com', 'projects.eclipse.org', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (22, 'johnsmith', 'smitherines@fake.com', 'marketplace.eclipse.org', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (22, 'johnsmith', 'smitherines@fake.com', 'blogs.eclipse.org', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (22, 'johnsmith', 'smitherines@fake.com', 'eclipse.org/downloads/packages', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (22, 'johnsmith', 'smitherines@fake.com', 'newsroom.eclipse.org', 1, 134567891, 134567891);

-- This person should not return as there are 2 with 'status = 0'
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (33, 'jasonbourne', 'ultimatum@fake.com', 'accounts.eclipse.org', 0, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (33, 'jasonbourne', 'ultimatum@fake.com', 'projects.eclipse.org', 2, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (33, 'jasonbourne', 'ultimatum@fake.com', 'marketplace.eclipse.org', 0, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (33, 'jasonbourne', 'ultimatum@fake.com', 'blogs.eclipse.org', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (33, 'jasonbourne', 'ultimatum@fake.com', 'eclipse.org/downloads/packages', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (33, 'jasonbourne', 'ultimatum@fake.com', 'newsroom.eclipse.org', 1, 134567891, 134567891);

-- This person is considered to be complete. All status set to 1 or 2
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (44, 'spongebob', 'spongebob@bikinibottom.com', 'accounts.eclipse.org', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (44, 'spongebob', 'spongebob@bikinibottom.com', 'projects.eclipse.org', 2, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (44, 'spongebob', 'spongebob@bikinibottom.com', 'marketplace.eclipse.org', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (44, 'spongebob', 'spongebob@bikinibottom.com', 'blogs.eclipse.org', 1, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (44, 'spongebob', 'spongebob@bikinibottom.com', 'eclipse.org/downloads/packages', 2, 134567891, 134567891);
INSERT INTO user_delete_request(`uid`, `name`, mail, host, `status`, created, changed)
  VALUES (44, 'spongebob', 'spongebob@bikinibottom.com', 'newsroom.eclipse.org', 1, 134567891, 134567891);
