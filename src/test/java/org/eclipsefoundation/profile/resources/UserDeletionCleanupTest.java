/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.profile.dtos.eclipseapi.CompletedUserDeleteRequest;
import org.eclipsefoundation.profile.tasks.UserDeletionCleanup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;

@QuarkusTest
@Order(1)
class UserDeletionCleanupTest {

    @Inject
    DefaultHibernateDao defaultDao;
    @Inject
    FilterService filters;
    @Inject
    UserDeletionCleanup cleanupTask;
    @ConfigProperty(name = "eclipse.profile.user-delete.hosts")
    Map<String, String> hosts;

    @Test
    void performDeletionRequestCleanup_success() {
        RequestWrapper wrap = new FlatRequestWrapper(URI.create("api.eclipse.org"));
        RDBMSQuery<CompletedUserDeleteRequest> query = new RDBMSQuery<>(wrap, filters.get(CompletedUserDeleteRequest.class),
                new MultivaluedHashMap<>());
        query.setUseLimit(false);
        List<CompletedUserDeleteRequest> results = defaultDao.get(query);

        int expectedSize = hosts.keySet().size() - 1;
        
        Assertions.assertFalse(results.isEmpty());
        Assertions.assertEquals(expectedSize, results.size());

        // Run cleanup and check that they're all gone
        cleanupTask.performDeletionRequestCleanup();
        results = defaultDao.get(query);
        Assertions.assertTrue(results.isEmpty());
        Assertions.assertEquals(0, results.size());
    }
}
