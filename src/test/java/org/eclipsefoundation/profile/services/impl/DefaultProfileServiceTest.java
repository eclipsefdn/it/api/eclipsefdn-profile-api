/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
/**
 * 
 */
package org.eclipsefoundation.profile.services.impl;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.http.exception.ApplicationException;
import org.eclipsefoundation.profile.services.ProfileService;
import org.eclipsefoundation.profile.test.api.MockAccountsAPI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@QuarkusTest
class DefaultProfileServiceTest {

    @Inject
    CachingService cache;
    @Inject
    ProfileService ps;

    @InjectMock
    @RestClient
    MockAccountsAPI apimock;

    /**
     * Regression test for #110, check that we can handle 404's w/ unexpected return values properly
     */
    @Test
    void getProfileByUsername_failure_handlesServiceDown() {
        String username = "username_notfound";

        // reset the mock between the tests
        Mockito.reset(apimock);
        // change to throw the equivalent of a service down page (a 404 HTML page)
        Mockito
                .when(apimock.getAccountsProfileByUsername(Mockito.anyString(), Mockito.anyString()))
                .thenThrow(new WebApplicationException(Response
                        .status(Status.NOT_FOUND.getStatusCode())
                        .entity("<html><body><h1>ooga booga I broke</h1></body></html>")
                        .type(MediaType.TEXT_HTML)
                        .build()));
        // this should throw
        Assertions
                .assertThrows(ApplicationException.class, () -> ps.getProfileByUsername(username),
                        "Did not throw an ApplicationException as expected");
    }

    @Test
    void getProfileByUsername_success_handlesMissingUsers() {
        String username = "username_notfound";
        // check that we can handle a not found user
        Mockito.when(apimock.getAccountsProfileByUsername(Mockito.anyString(), Mockito.anyString())).thenCallRealMethod();
        Assertions.assertNull(ps.getProfileByUsername(username));
    }
}
