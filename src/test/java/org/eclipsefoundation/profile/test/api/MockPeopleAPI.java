/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullPeopleProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullPeopleProjectDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleEmailsData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleEmailsDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.project.ProjectDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationDataBuilder;
import org.eclipsefoundation.profile.api.PeopleAPI;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.WebApplicationException;

@Mock
@RestClient
@ApplicationScoped
public class MockPeopleAPI implements PeopleAPI {

    private List<FullPeopleProjectData> peopleProjects;
    private List<PeopleDocumentData> peopleDocs;
    private List<PeopleEmailsData> peopleEmails;
    private List<PeopleData> people;

    public MockPeopleAPI() {

        this.peopleProjects = new ArrayList<>();
        this.peopleProjects
                .addAll(Arrays
                        .asList(createPeopleProject("firstlast", "some.project", "CM"),
                                createPeopleProject("firstlast", "other.project", "ME"),
                                createPeopleProject("username", "some.project", "CM"),
                                createPeopleProject("barshallblathers", "some.project", "ME"),
                                createPeopleProject("username", "other.project", "CM")));

        this.peopleDocs = new ArrayList<>();
        this.peopleDocs
                .addAll(Arrays
                        .asList(PeopleDocumentDataBuilder
                                .builder()
                                .personID("firstlast")
                                .documentID("ecaId")
                                .version(3.0)
                                .effectiveDate(new Date())
                                .build(),
                                PeopleDocumentDataBuilder
                                        .builder()
                                        .personID("firstlast")
                                        .documentID("iwgpa-id")
                                        .version(1.0)
                                        .effectiveDate(new Date())
                                        .build(),
                                PeopleDocumentDataBuilder
                                        .builder()
                                        .personID("username")
                                        .documentID("openvsx")
                                        .version(1.0)
                                        .effectiveDate(new Date())
                                        .build(),
                                PeopleDocumentDataBuilder
                                        .builder()
                                        .personID("fakeperson")
                                        .documentID("claId")
                                        .version(2.0)
                                        .effectiveDate(new Date())
                                        .build(),
                                PeopleDocumentDataBuilder
                                        .builder()
                                        .personID("barshallblathers")
                                        .documentID("ecaId")
                                        .version(1.0)
                                        .effectiveDate(new Date())
                                        .build(),
                                PeopleDocumentDataBuilder
                                        .builder()
                                        .personID("barshallblathers")
                                        .documentID("icaId")
                                        .version(1.0)
                                        .effectiveDate(new Date())
                                        .build()));

        this.peopleEmails = new ArrayList<>();
        this.peopleEmails
                .addAll(Arrays
                        .asList(PeopleEmailsDataBuilder.builder().emailID(1).personID("username").email("fake@mail.org").build(),
                                PeopleEmailsDataBuilder.builder().emailID(2).personID("username").email("also-fake@test.org").build(),
                                PeopleEmailsDataBuilder.builder().emailID(1).personID("firstlast").email("test@false.com").build(),
                                PeopleEmailsDataBuilder.builder().emailID(1).personID("barshallblathers").email("fake@email.org").build(),
                                PeopleEmailsDataBuilder.builder().emailID(1).personID("username").email("duplicated@email.org").build(),
                                PeopleEmailsDataBuilder.builder().emailID(1).personID("firstlast").email("duplicated@email.org").build()));

        this.people = new ArrayList<>();
        this.people
                .addAll(Arrays
                        .asList(createPerson("username"), createPerson("firstlast"), createPerson("barshallblathers"),
                                createPerson("fakeperson")));
    }

    @Override
    public List<FullPeopleProjectData> getFullPeopleProjects(String personId, boolean isActive) {
        List<FullPeopleProjectData> results = peopleProjects
                .stream()
                .filter(p -> p.person().personID().equalsIgnoreCase(personId))
                .toList();
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }

    @Override
    public List<PeopleDocumentData> getDocuments(String personId, boolean isNotExpired, boolean includeBytes) {
        List<PeopleDocumentData> results = peopleDocs.stream().filter(p -> p.personID().equalsIgnoreCase(personId)).toList();
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }

    @Override
    public List<PeopleEmailsData> getEmails(String personId) {
        List<PeopleEmailsData> results = peopleEmails.stream().filter(e -> e.personID().equalsIgnoreCase(personId)).toList();
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }

    @Override
    public List<PeopleEmailsData> getEmailsByEmail(String email) {
        List<PeopleEmailsData> results = peopleEmails.stream().filter(e -> e.email().equalsIgnoreCase(email)).toList();
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }

    @Override
    public PeopleData getPerson(String personId) {
        List<PeopleData> results = people.stream().filter(e -> e.personID().equalsIgnoreCase(personId)).toList();
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results.get(0);
    }

    private FullPeopleProjectData createPeopleProject(String personId, String projectId, String relation) {
        return FullPeopleProjectDataBuilder
                .builder()
                .person(createPerson(personId))
                .project(ProjectDataBuilder
                        .builder()
                        .projectID(projectId)
                        .name("project name")
                        .level(2)
                        .active(true)
                        .parentProjectID("some.id")
                        .description("desc....")
                        .urlDownload("download here")
                        .urlIndex("index")
                        .sortOrder(0)
                        .diskQuotaGB(0)
                        .component(false)
                        .standard(true)
                        .build())
                .relation(SysRelationDataBuilder.builder().relation(relation).description("some role").active(true).type("PR").build())
                .activeDate(new Date())
                .editBugs(false)
                .build();
    }

    private PeopleData createPerson(String personId) {
        return PeopleDataBuilder
                .builder()
                .personID(personId)
                .fname("first")
                .lname("last")
                .type("XX")
                .email("null")
                .member(true)
                .unixAcctCreated(true)
                .issuesPending(false)
                .comments("alt-email:testing.email@google.com\r\nalt-email:" + personId + "@test.com\r\n")
                .build();
    }
}
