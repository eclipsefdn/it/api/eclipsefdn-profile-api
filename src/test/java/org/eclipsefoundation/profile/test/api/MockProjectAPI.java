/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.ProjectsAPI;
import org.eclipsefoundation.efservices.api.models.GitlabProjectBuilder;
import org.eclipsefoundation.efservices.api.models.InterestGroup;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.api.models.Project.ProjectParticipant;
import org.eclipsefoundation.efservices.api.models.Project.Repo;
import org.eclipsefoundation.efservices.api.models.ProjectBuilder;
import org.eclipsefoundation.efservices.api.models.ProjectGithubProjectBuilder;
import org.eclipsefoundation.efservices.api.models.ProjectProjectParticipantBuilder;
import org.eclipsefoundation.efservices.api.models.ProjectRepoBuilder;
import org.eclipsefoundation.testing.helpers.MockDataPaginationHandler;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.test.Mock;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;

@Mock
@RestClient
@ApplicationScoped
public class MockProjectAPI implements ProjectsAPI {

    private List<Project> specProjects;

    public MockProjectAPI() {
        this.specProjects = new ArrayList<>();

        // sample repos
        Repo r1 = ProjectRepoBuilder.builder().url("http://www.github.com/eclipsefdn/sample").build();
        Repo r2 = ProjectRepoBuilder.builder().url("http://www.github.com/eclipsefdn/test").build();
        Repo r3 = ProjectRepoBuilder.builder().url("http://www.github.com/eclipsefdn/tck-proto").build();
        Repo r4 = ProjectRepoBuilder.builder().url("/gitroot/sample/gerrit.project.git").build();

        // sample users
        ProjectParticipant u1 = ProjectProjectParticipantBuilder.builder().url("").username("da_wizz").fullName("da_wizz").build();
        ProjectParticipant u2 = ProjectProjectParticipantBuilder.builder().url("").username("grunter").fullName("grunter").build();

        this.specProjects
                .add(ProjectBuilder
                        .builder()
                        .name("Some project")
                        .projectId("some.project")
                        .shortProjectId("sample.proj")
                        .summary("summary")
                        .url("project.url.com")
                        .websiteUrl("someproject.com")
                        .websiteRepo(Collections.emptyList())
                        .logo("logoUrl.com")
                        .tags(Collections.emptyList())
                        .githubRepos(Arrays.asList(r1, r2))
                        .gitlabRepos(Collections.emptyList())
                        .gerritRepos(Arrays.asList(r4))
                        .committers(Arrays.asList(u1, u2))
                        .projectLeads(Collections.emptyList())
                        .gitlab(GitlabProjectBuilder.builder().ignoredSubGroups(Collections.emptyList()).projectGroup("").build())
                        .github(ProjectGithubProjectBuilder.builder().org("org name").ignoredRepos(Collections.emptyList()).build())
                        .contributors(Collections.emptyList())
                        .industryCollaborations(Collections.emptyList())
                        .releases(Collections.emptyList())
                        .topLevelProject("eclipse")
                        .specProjectWorkingGroup(Map.of("id", "proj1", "name", "proj1"))
                        .build());

        this.specProjects
                .add(ProjectBuilder
                        .builder()
                        .name("Other project")
                        .projectId("other.project")
                        .specProjectWorkingGroup(Map.of("id", "proj1", "name", "proj1"))
                        .githubRepos(Arrays.asList(r3))
                        .gitlabRepos(Collections.emptyList())
                        .gerritRepos(Collections.emptyList())
                        .gitlab(GitlabProjectBuilder
                                .builder()
                                .ignoredSubGroups(Arrays.asList("eclipse/dash/mirror"))
                                .projectGroup("eclipse/dash")
                                .build())
                        .committers(Arrays.asList(u1, u2))
                        .projectLeads(Collections.emptyList())
                        .shortProjectId("spec.proj")
                        .summary("summary")
                        .url("project.url.com")
                        .websiteUrl("someproject.com")
                        .websiteRepo(Collections.emptyList())
                        .logo("logoUrl.com")
                        .tags(Collections.emptyList())
                        .github(ProjectGithubProjectBuilder.builder().org("org name").ignoredRepos(Collections.emptyList()).build())
                        .contributors(Collections.emptyList())
                        .industryCollaborations(Collections.emptyList())
                        .releases(Collections.emptyList())
                        .topLevelProject("eclipse")
                        .build());
    }

    @Override
    public Uni<RestResponse<List<Project>>> getProjects(BaseAPIParameters params, int isSpecProject) {
        List<Project> out = specProjects;
        if (isSpecProject == 1) {
            out = out.stream().filter(p -> p.getSpecWorkingGroup().isPresent()).toList();
        }

        return Uni.createFrom().item(MockDataPaginationHandler.paginateData(params, out));
    }

    @Override
    public Uni<RestResponse<List<InterestGroup>>> getInterestGroups(BaseAPIParameters params) {
        return Uni.createFrom().item(RestResponse.ok(Collections.emptyList()));
    }
}
