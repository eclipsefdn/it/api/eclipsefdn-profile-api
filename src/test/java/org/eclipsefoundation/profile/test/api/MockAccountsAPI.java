/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.api;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.models.EfUserCountryBuilder;
import org.eclipsefoundation.profile.api.DrupalAccountsAPI;
import org.eclipsefoundation.profile.api.models.AccountsProfileData;
import org.eclipsefoundation.profile.helpers.ProfileHelper;
import org.eclipsefoundation.profile.test.helpers.TestNamespaceHelper;
import org.jboss.resteasy.reactive.common.jaxrs.ResponseImpl;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response.Status;

@Mock
@RestClient
@ApplicationScoped
public class MockAccountsAPI implements DrupalAccountsAPI {

    public static final String USERNAME_NO_LDAP_USER = "noldap_user";
    public static final String LEGACY_NOT_FOUND_MESSAGE = String.format("[\"%s\"]", ProfileHelper.USER_NOT_FOUND_MESSAGE);
    public static final String MODERN_NOT_FOUND_MESSAGE = String.format("{\"message\":\"%s\"}", ProfileHelper.USER_NOT_FOUND_MESSAGE);

    // switched to static to better handle mocks
    private static final List<AccountsProfileData> USERS = Arrays
            .asList(AccountsProfileData
                    .builder()
                    .setUid("666")
                    .setName("username")
                    .setFirstName("user")
                    .setLastName("name")
                    .setFullName("user name")
                    .setTwitterHandle("handle")
                    .setOrg("Eclipse")
                    .setJobTitle("boss")
                    .setWebsite("google.com")
                    .setCountry(EfUserCountryBuilder.builder().code("CA").name("Canada").build())
                    .setBio("Likes do do things")
                    .setInterests(Arrays.asList("item1", "item2"))
                    .setPicture("pic URL")
                    .setMxid("")
                    .build(),
                    AccountsProfileData
                            .builder()
                            .setUid("42")
                            .setName("firstlast")
                            .setFirstName("first")
                            .setLastName("last")
                            .setFullName("first last")
                            .setTwitterHandle("twit")
                            .setOrg("IBM")
                            .setJobTitle("Worker")
                            .setWebsite("google.com")
                            .setCountry(EfUserCountryBuilder.builder().code("US").name("United States").build())
                            .setBio("Likes do do things")
                            .setInterests(Arrays.asList("item1", "item2"))
                            .setPicture("pic URL")
                            .setMxid("")
                            .build(),
                    AccountsProfileData
                            .builder()
                            .setUid("77")
                            .setName("fakeperson")
                            .setFirstName("fake")
                            .setLastName("person")
                            .setFullName("fake person")
                            .setTwitterHandle("tweets")
                            .setOrg("Eclipse")
                            .setJobTitle("Worker")
                            .setWebsite("google.com")
                            .setCountry(EfUserCountryBuilder.builder().code("CA").name("Canada").build())
                            .setBio("Likes do do things")
                            .setInterests(Arrays.asList("item1", "item2"))
                            .setPicture("pic URL")
                            .setMxid("")
                            .build(),
                    AccountsProfileData
                            .builder()
                            .setUid("88")
                            .setName("barshallblathers")
                            .setFirstName("barshall")
                            .setLastName("blathers")
                            .setFullName("barshall blathers")
                            .setTwitterHandle("twitter@handle")
                            .setOrg("Microsoft")
                            .setJobTitle("Worker")
                            .setWebsite("google.com")
                            .setCountry(EfUserCountryBuilder.builder().code("UK").name("United Kingdom").build())
                            .setBio("Likes do do things")
                            .setInterests(Arrays.asList("item1", "item2"))
                            .setPicture("pic URL")
                            .setMxid("")
                            .build(),
                    AccountsProfileData
                            .builder()
                            .setUid("99")
                            .setName(USERNAME_NO_LDAP_USER)
                            .setFirstName("test")
                            .setLastName("test")
                            .setFullName("test test ")
                            .setTwitterHandle("twitter@handle")
                            .setOrg("Microsoft")
                            .setJobTitle("Worker")
                            .setWebsite("google.com")
                            .setCountry(EfUserCountryBuilder.builder().code("UK").name("United Kingdom").build())
                            .setBio("Likes do do things")
                            .setInterests(Arrays.asList("item1", "item2"))
                            .setPicture("pic URL")
                            .setMxid("")
                            .build());

    @Override
    public AccountsProfileData getAccountsProfileByUsername(String username, String bearerToken) {
        // Iss #109, throw errors with a given username for testing
        if (username.startsWith(TestNamespaceHelper.ERROR_TRIGGER_PREFIX)) {
            throw new WebApplicationException(Integer.parseInt(username.substring(TestNamespaceHelper.ERROR_TRIGGER_PREFIX.length())));
        }
        Optional<AccountsProfileData> result = USERS.stream().filter(u -> u.getName().equalsIgnoreCase(username)).findFirst();
        if (result.isEmpty()) {
            // needs this in particular, as base response doesn't set stream like reactive lib does
            ResponseImpl r = new ResponseImpl();
            r
                    .setEntityStream(
                            TestNamespaceHelper.USERNAME_LEGACY_NOT_FOUND.equals(username) ? new ByteArrayInputStream(LEGACY_NOT_FOUND_MESSAGE.getBytes())
                                    : new ByteArrayInputStream(MODERN_NOT_FOUND_MESSAGE.getBytes()));
            r.setStatus(Status.NOT_FOUND.getStatusCode());
            throw new WebApplicationException(r);
        }
        return result.get();
    }

    @Override
    public List<AccountsProfileData> getAccountsProfileByUid(String uid, String bearerToken) {
        if (uid.startsWith(TestNamespaceHelper.ERROR_TRIGGER_PREFIX)) {
            throw new WebApplicationException(Integer.parseInt(uid.substring(TestNamespaceHelper.ERROR_TRIGGER_PREFIX.length())));
        }
        List<AccountsProfileData> results = USERS.stream().filter(u -> u.getUid().equalsIgnoreCase(uid)).toList();
        if (results.isEmpty()) {
            // needs this in particular, as base response doesn't set stream like reactive lib does
            ResponseImpl r = new ResponseImpl();
            r
                    .setEntityStream(TestNamespaceHelper.USERNAME_LEGACY_NOT_FOUND.equals(uid) ? new ByteArrayInputStream(LEGACY_NOT_FOUND_MESSAGE.getBytes())
                            : new ByteArrayInputStream(MODERN_NOT_FOUND_MESSAGE.getBytes()));
            r.setStatus(Status.NOT_FOUND.getStatusCode());
            throw new WebApplicationException(r);
        }
        return results;
    }

}
