/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationContactDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationDocumentDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysRelationDataBuilder;
import org.eclipsefoundation.profile.api.OrganizationAPI;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.WebApplicationException;

@Mock
@RestClient
@ApplicationScoped
public class MockOrganizationAPI implements OrganizationAPI {

    private List<OrganizationDocumentData> documents;
    private List<FullOrganizationContactData> fullOrgContacts;
    private List<OrganizationContactData> orgContacts;

    public MockOrganizationAPI() {
        this.documents = new ArrayList<>();
        this.documents
                .addAll(Arrays
                        .asList(OrganizationDocumentDataBuilder.builder().organizationID(42).documentID("mccaId").version(1).build(),
                                OrganizationDocumentDataBuilder
                                        .builder()
                                        .organizationID(42)
                                        .documentID("someOtherDoc")
                                        .version(1)
                                        .build()));

        this.orgContacts = new ArrayList<>();
        this.orgContacts
                .addAll(Arrays
                        .asList(OrganizationContactDataBuilder
                                .builder()
                                .organizationID(42)
                                .personID("username")
                                .relation("EMPLY")
                                .documentID(null)
                                .comments("PRIMARY")
                                .title("Head honcho")
                                .build()));

        this.fullOrgContacts = new ArrayList<>();
        this.fullOrgContacts
                .addAll(Arrays
                        .asList(FullOrganizationContactDataBuilder
                                .builder()
                                .organization(OrganizationDataBuilder
                                        .builder()
                                        .organizationID(42)
                                        .name1("LovelyOrg")
                                        .name2("")
                                        .phone("911")
                                        .fax("911-2")
                                        .comments("Lovely org")
                                        .timestamp(new Date())
                                        .build())
                                .person(PeopleDataBuilder
                                        .builder()
                                        .personID("username")
                                        .fname("user")
                                        .lname("name")
                                        .type("XX")
                                        .member(false)
                                        .email("email@fake.com")
                                        .unixAcctCreated(false)
                                        .issuesPending(false)
                                        .build())
                                .documentID(null)
                                .sysRelation(SysRelationDataBuilder
                                        .builder()
                                        .relation("EMPLY")
                                        .description("Employee")
                                        .active(true)
                                        .type("CO")
                                        .build())
                                .comments("PRIMARY")
                                .title("Head honcho")
                                .build()));
    }

    @Override
    public List<OrganizationDocumentData> getOrganizationDocuments(String orgId, boolean isNotExpired, boolean includeBytes) {
        List<OrganizationDocumentData> results = documents.stream().filter(d -> d.organizationID() == Integer.valueOf(orgId)).toList();
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }

    @Override
    public List<FullOrganizationContactData> getFullOrgContacts(String username) {
        List<FullOrganizationContactData> results = fullOrgContacts
                .stream()
                .filter(c -> c.person().personID().equalsIgnoreCase(username))
                .toList();
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }

    @Override
    public List<OrganizationContactData> getOrgContacts(String username) {
        List<OrganizationContactData> results = orgContacts.stream().filter(c -> c.personID().equalsIgnoreCase(username)).toList();
        if (results.isEmpty()) {
            throw new WebApplicationException(404);
        }
        return results;
    }
}
