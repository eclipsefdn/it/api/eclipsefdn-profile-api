/*********************************************************************
* Copyright (c) 2024 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.tasks;

import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.profile.dtos.eclipseapi.CompletedUserDeleteRequest;
import org.eclipsefoundation.profile.dtos.eclipseapi.UserDeleteRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

@ApplicationScoped
public class UserDeletionCleanup {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDeletionCleanup.class);

    @ConfigProperty(name = "eclipse.profile.scheduled.deletion-cleanup.enabled", defaultValue = "true")
    Instance<Boolean> enabled;

    @Inject
    DefaultHibernateDao defaultDao;
    @Inject
    FilterService filters;

    @Scheduled(every = "PT1H", delay = 10, delayUnit = TimeUnit.SECONDS)
    public void performDeletionRequestCleanup() {

        if (Boolean.FALSE.equals(enabled.get())) {
            LOGGER.info("User Deletion Request cleanup task disabled through configuration.");
        } else {
            LOGGER.info("Cleanup Task - Scanning for completed requests");

            RequestWrapper wrap = new FlatRequestWrapper(URI.create("api.eclipse.org"));
            RDBMSQuery<CompletedUserDeleteRequest> query = new RDBMSQuery<>(wrap, filters.get(CompletedUserDeleteRequest.class),
                    new MultivaluedHashMap<>());
            query.setUseLimit(false);
            List<CompletedUserDeleteRequest> results = defaultDao.get(query);

            LOGGER.info("Cleanup Task - Found {} results", results.size());

            if (!results.isEmpty()) {
                MultivaluedMap<String, String> params = new MultivaluedHashMap<>();

                results.stream().forEach(req -> {
                    params.add(DefaultUrlParameterNames.ID.getName(), Long.toString(req.getId()));
                    defaultDao.delete(new RDBMSQuery<>(wrap, filters.get(UserDeleteRequest.class), params));
                    params.clear();
                    LOGGER.info("Cleanup Task - Deleted Request for user '{}', host '{}'", req.getName(), req.getHost());
                });
            }
        }
    }
}
