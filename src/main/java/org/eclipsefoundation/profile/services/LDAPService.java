/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.services;

import java.util.Optional;

import org.eclipsefoundation.profile.models.LdapResult;

/**
 * Defines the service used to perform LDAP searches using specific fields.
 */
public interface LDAPService {

    /**
     * Performs an LDAP search, mathing via the username. Returns an empty LdapResult if no result found.
     * 
     * @param efUsername The desired EF username.
     * @return An Optional containing an LdapResult entity if it exists.
     */
    Optional<LdapResult> searchLdapByUsername(String efUsername);

    /**
     * Performs an LDAP search, matching via the the GH id. Returns an empty LdapResult if no result found.
     * 
     * @param ghHandle the desired GH id.
     * @return An Optional containing an LdapResult entity if it exists.
     */
    Optional<LdapResult> searchLdapByGhHandle(String ghHandle);

    /**
     * Performs an LDAP search, mathing via the email. Returns an empty LdapResult if no result found.
     * 
     * @param email The desired user email.
     * @return An Optional containing an LdapResult entity if it exists.
     */
    Optional<LdapResult> searchLdapByEmail(String email);
}
