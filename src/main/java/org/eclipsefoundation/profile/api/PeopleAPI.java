/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullPeopleProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleEmailsData;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * FoundationDb-API binding for '/people'
 */
@Path("/people")
@OidcClientFilter
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
@RegisterRestClient(configKey = "fdndb-api")
public interface PeopleAPI {

    /**
     * Gets all FullPeopleProjectData entities tied to the given personId.
     * 
     * @param personId The given personId.
     * @param isActive Flag to enable only active relations
     * @return A List of FullPeopleProjectData entities if they exist.
     */
    @GET
    @Path("{personId}/projects/full")
    List<FullPeopleProjectData> getFullPeopleProjects(@PathParam("personId") String personId, @QueryParam("is_active") boolean isActive);

    /**
     * Gets all PeopleDocumentData entities tied to the given personId.
     * 
     * @param personId The given personId.
     * @param isNotExpired Flag to enable non-expired documents
     * @return A List of PeopleDocumentData entities if they exist.
     */
    @GET
    @Path("/{personId}/documents")
    List<PeopleDocumentData> getDocuments(@PathParam("personId") String personId, @QueryParam("is_not_expired") boolean isNotExpired,
            @QueryParam("include_bytes") boolean includeBytes);

    /**
     * Gets all PeopleEmailData entities tied to the given personId.
     * 
     * @param personId The given personId.
     * @return A List of PeopleEmailData entities if they exist.
     */
    @GET
    @Path("/{personId}/emails")
    List<PeopleEmailsData> getEmails(@PathParam("personId") String personId);

    /**
     * Gets all PeopleEmailData entities tied to the given email address. Used to do reverse lookups for historical emails
     * 
     * @param email The historical email for a potential user.
     * @return A List of PeopleEmailData entities if they exist.
     */
    @GET
    @Path("/emails")
    List<PeopleEmailsData> getEmailsByEmail(@QueryParam("email") String email);

    /**
     * Gets a People entity tied to the given personId.
     * 
     * @param personId The given personId.
     * @return A People entity if it exists.
     */
    @GET
    @Path("/{personId}")
    PeopleData getPerson(@PathParam("personId") String personId);
}
