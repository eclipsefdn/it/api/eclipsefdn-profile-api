/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.response;

import java.io.IOException;
import java.util.Collections;

import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUserBuilder;
import org.eclipsefoundation.efservices.api.models.EfUserCountryBuilder;
import org.eclipsefoundation.efservices.models.AuthenticatedRequestWrapper;
import org.jboss.resteasy.reactive.server.ServerResponseFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;

public class ProfileFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileFilter.class);

    @Inject
    AuthenticatedRequestWrapper tokenWrap;

    @ServerResponseFilter
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {

        // Anonymize private fields if response is an EfUser entity
        if (responseContext.getEntity() instanceof EfUser profile && !tokenWrap.isAuthenticated()) {
            responseContext
                    .setEntity(EfUserBuilder.builder(profile)
                            .mail("")
                            .mxid("")
                            .country(EfUserCountryBuilder.builder().code(null).name(null).build())
                            .mailAlternate(Collections.emptyList())
                            .mailHistory(Collections.emptyList())
                            .build());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Profile anonymized for user: {}", profile.name());
            }
        }
    }
}
