/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.dtos.eclipse;

import java.util.Date;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "friends")
public class Friends extends BareNode {
    public static final DtoTable TABLE = new DtoTable(Friends.class, "frnd");

    @Id
    private Integer friendId;
    private Integer bugzillaId;
    private Date dateJoined;
    private Boolean isAnonymous;
    private String firstName;
    private String lastName;
    private Boolean isBenefit;
    private String uid;

    @JsonIgnore
    @Override
    public Object getId() {
        return getFriendId();
    }

    public Integer getFriendId() {
        return this.friendId;
    }

    public void setFriendId(Integer id) {
        this.friendId = id;
    }

    public Integer getBugzillaId() {
        return this.bugzillaId;
    }

    public void setBugzillaId(Integer id) {
        this.bugzillaId = id;
    }

    public Date getDateJoined() {
        return this.dateJoined;
    }

    public void setDateJoined(Date dateJoined) {
        this.dateJoined = dateJoined;
    }

    public Boolean getIsAnonymous() {
        return this.isAnonymous;
    }

    public void setIsAnonymous(Boolean isAnonymous) {
        this.isAnonymous = isAnonymous;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getIsBenefit() {
        return this.isBenefit;
    }

    public void setIsBenefit(Boolean isBenefit) {
        this.isBenefit = isBenefit;
    }

    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void anonymizeEntity() {
        this.uid = null;
        this.isAnonymous = true;
        this.firstName = "";
        this.lastName = "";
        this.bugzillaId = null;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(friendId, bugzillaId, dateJoined, isAnonymous, firstName, lastName, isBenefit, uid);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Friends other = (Friends) obj;
        return Objects.equals(friendId, other.friendId) && Objects.equals(bugzillaId, other.bugzillaId)
                && Objects.equals(dateJoined, other.dateJoined) && Objects.equals(isAnonymous, other.isAnonymous)
                && Objects.equals(firstName, other.firstName) && Objects.equals(lastName, other.lastName)
                && Objects.equals(isBenefit, other.isBenefit) && Objects.equals(uid, other.uid);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Friends [friend_id=");
        builder.append(friendId);
        builder.append(", bugzilla_id=");
        builder.append(bugzillaId);
        builder.append(", date_joined=");
        builder.append(dateJoined);
        builder.append(", is_anonymous=");
        builder.append(isAnonymous);
        builder.append(", first_name=");
        builder.append(firstName);
        builder.append(", last_name=");
        builder.append(lastName);
        builder.append(", is_benefit=");
        builder.append(isBenefit);
        builder.append(", uid=");
        builder.append(uid);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class FriendsFilter implements DtoFilter<Friends> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);
            if (isRoot) {
                String friendId = params.getFirst(ProfileAPIParameterNames.FRIEND_ID.getName());
                if (friendId != null) {
                    statement
                            .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".friendId = ?", new Object[] { friendId }));
                }
            }

            String uid = params.getFirst(ProfileAPIParameterNames.UID.getName());
            if (uid != null) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".uid = ?", new Object[] { uid }));
            }

            return statement;
        }

        @Override
        public Class<Friends> getType() {
            return Friends.class;
        }
    }
}
