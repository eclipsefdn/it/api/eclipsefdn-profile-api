/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.dtos.eclipseapi;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;
import org.hibernate.annotations.Synchronize;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * Immutable DTO for the user_delete_request table. Used to perform a specific SELECT query.
 */
@Entity
@Immutable
@Synchronize({ "user_delete_request" })
@Subselect("""
    SELECT *
    FROM user_delete_request del
    WHERE del.status = 0
    AND del.name IN (
        SELECT del2.name
        FROM user_delete_request del2
        WHERE del2.status = 0
        GROUP BY del2.name
        HAVING COUNT(del2.name) = 1)""")
public class FinalStageUserDeleteRequest extends BareNode {
    public static final DtoTable TABLE = new DtoTable(FinalStageUserDeleteRequest.class, "udr");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int uid;
    private String name;
    private String mail;
    private String host;
    private int status;
    private int created;
    private int changed;

    @Override
    public Long getId() {
        return id;
    }

    public int getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getHost() {
        return host;
    }

    public int getStatus() {
        return status;
    }

    public int getCreated() {
        return created;
    }

    public int getChanged() {
        return changed;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(id, uid, name, mail, host, status, changed, created);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        FinalStageUserDeleteRequest other = (FinalStageUserDeleteRequest) obj;
        return Objects.equals(id, other.id) && Objects.equals(uid, other.uid) && Objects.equals(name, other.name)
                && Objects.equals(mail, other.mail) && Objects.equals(host, other.host) && Objects.equals(status, other.status)
                && Objects.equals(changed, other.changed) && Objects.equals(created, other.created);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FinalStageUserDeleteRequest [id=");
        builder.append(id);
        builder.append(", uid=");
        builder.append(uid);
        builder.append(", name=");
        builder.append(name);
        builder.append(", mail=");
        builder.append(mail);
        builder.append(", host=");
        builder.append(host);
        builder.append(", status=");
        builder.append(status);
        builder.append(", changed=");
        builder.append(changed);
        builder.append(", created=");
        builder.append(created);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class UserDeleteRequestFilter implements DtoFilter<FinalStageUserDeleteRequest> {
        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            String id = params.getFirst(DefaultUrlParameterNames.ID.getName());
            if (StringUtils.isNumeric(id)) {
                statement.addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id = ?", new Object[] { Long.valueOf(id) }));
            }

            return statement;
        }

        @Override
        public Class<FinalStageUserDeleteRequest> getType() {
            return FinalStageUserDeleteRequest.class;
        }
    }
}
