/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.helpers;

import java.time.Instant;
import java.util.Date;

import org.eclipsefoundation.http.helper.IPParser;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.profile.dtos.eclipse.AccountRequests;
import org.eclipsefoundation.profile.dtos.eclipse.AccountRequests.AccountRequestsCompositeID;
import org.eclipsefoundation.utils.helper.TransformationHelper;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * A helper class used build various entities and process data related to user delete requests.
 */
@ApplicationScoped
public final class UserDeleteHelper {
    
    private final TransformationHelper transformHelper;
    private final IPParser parser;

    public UserDeleteHelper(TransformationHelper helper, IPParser parser) {
        this.transformHelper = helper;
        this.parser = parser;
    }

    /**
     * Builds a MultivaluedMap containing the desired ID param.
     * 
     * @param id The given id.
     * @return A MultivaluedMap containing the parsed int as a param.
     */
    public MultivaluedMap<String, String> buildIdParams(int id) {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID.getName(), Integer.toString(id));
        return params;
    }

    /**
     * Builds an AccountRequests entity used to flag that a deletion request has been made.
     * 
     * @param wrap The current request wrapper
     * @param email The given user email
     * @param name The given username.
     * @return A populated AccountRequests entity.
     */
    public AccountRequests buildDeletionAccountRequest(String email, String username) {
        AccountRequests request = new AccountRequests();
        AccountRequestsCompositeID compID = new AccountRequestsCompositeID();
        compID.setEmail(email);
        compID.setReqWhen(Date.from(Instant.now()));
        request.setCompositeID(compID);
        request.setNewEmail("");
        request.setFname("DELETEACCOUNT");
        request.setLname("DELETEACCOUNT");
        request.setPassword(transformHelper.encryptAndEncodeValue(email));
        request.setIp(parser.getBestMatchingIP());
        request.setToken(username);
        request.setPersonId("");
        return request;
    }
}
