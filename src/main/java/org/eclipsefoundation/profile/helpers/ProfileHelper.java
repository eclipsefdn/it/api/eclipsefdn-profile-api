/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.helpers;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.efservices.api.models.EfUser.Email;
import org.eclipsefoundation.efservices.api.models.EfUserEmailBuilder;
import org.eclipsefoundation.efservices.services.DrupalTokenService;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullOrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.full.FullPeopleProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleEmailsData;
import org.eclipsefoundation.http.exception.ApplicationException;
import org.eclipsefoundation.profile.api.DrupalAccountsAPI;
import org.eclipsefoundation.profile.api.GerritAPI;
import org.eclipsefoundation.profile.api.OrganizationAPI;
import org.eclipsefoundation.profile.api.PeopleAPI;
import org.eclipsefoundation.profile.api.models.AccountsProfileData;
import org.eclipsefoundation.profile.api.models.GerritChangeData;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.jboss.resteasy.reactive.common.jaxrs.ResponseImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * Helper class used to fetch from various sources for profile data
 */
@ApplicationScoped
public class ProfileHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileHelper.class);

    private static final Pattern ALTERNATE_EMAIL_PATTERN = Pattern.compile("alt-email\\s*:\\s*([\\w\\-\\.]+@[\\w\\-]+(\\.[\\w\\-]{2,4})+)");
    private static final int GERRIT_RESPONSE_START_INDEX = 4;
    private static final String ERROR_MSG = "Issue connecting to Accounts";

    public static final String USER_NOT_FOUND_MESSAGE = "User not found.";

    @RestClient
    PeopleAPI peopleAPI;
    @RestClient
    OrganizationAPI orgAPI;

    @RestClient
    GerritAPI gerritAPI;
    @RestClient
    DrupalAccountsAPI accountsAPI;

    private final DrupalTokenService tokenService;
    private final ObjectMapper objectMapper;

    public ProfileHelper(DrupalTokenService tokenService, ObjectMapper objectMapper) {
        this.tokenService = tokenService;
        this.objectMapper = objectMapper;
    }

    /**
     * Queries the accounts-api for profile data for the given user via username.
     * 
     * @param username The given username
     * @return An Optional containing the AccountsProfileData entity if it exists, or empty.
     */
    public Optional<AccountsProfileData> getAccountsProfileByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching Accounts data for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return Optional.of(accountsAPI.getAccountsProfileByUsername(username, getBearerToken()));
        } catch (WebApplicationException e) {
            // fix #113 - only return empty when user is missing, otherwise throw
            if (check404ForUserMissing(e.getResponse())) {
                int status = e.getResponse().getStatus();
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("ACCOUNTS-API -  No account results for user: {}", TransformationHelper.formatLog(username), e);
                } else {
                    LOGGER
                            .warn("ACCOUNTS-API - No account results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                    TransformationHelper.formatLog(username), status);
                }
                return Optional.empty();
            }
            // or else throw, as we don't want to process error requests
            throw new ApplicationException(ERROR_MSG, e);
        }
    }

    /**
     * Queries the accounts-api for profile data for the given user via drupal uid.
     * 
     * @param uid The given uid
     * @return An Optional containing the AccountsProfileData entity if it exists, or empty.
     */
    public Optional<AccountsProfileData> getAccountsProfileByUid(String uid) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching Accounts profile data for user with uid: {}", uid);
        }
        try {
            return Optional.of(accountsAPI.getAccountsProfileByUid(uid, getBearerToken()).get(0));
        } catch (WebApplicationException e) {
            // fix #113 - only return empty when user is missing, otherwise throw
            if (check404ForUserMissing(e.getResponse())) {
                int status = e.getResponse().getStatus();
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("ACCOUNTS-API -  No account results for user: {}", TransformationHelper.formatLog(uid), e);
                } else {
                    LOGGER
                            .warn("ACCOUNTS-API - No account results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                    TransformationHelper.formatLog(uid), status);
                }
                return Optional.empty();
            }
            // or else throw, as we don't want to process error requests
            throw new ApplicationException(ERROR_MSG, e);
        }
    }

    /**
     * Queries Fdndb-api for any active FullPeopleProject data for the given user.
     * 
     * @param username The given username
     * @return A List of FullPeopleProjectData or empty
     */
    public List<FullPeopleProjectData> getFullPeopleProjectsByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching PeopleProjects for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return peopleAPI.getFullPeopleProjects(username, true).stream().toList();
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No PeopleProject results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No PeopleProject for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for any OrganizationContactData data for the given user.
     * 
     * @param username The given username
     * @return A List of OrganizationContactData or empty
     */
    public List<OrganizationContactData> getOrgContactByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching OrganizationContactData for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return orgAPI.getOrgContacts(username);
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No OrganizationContact results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No OrganizationContact results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for any FullOrganizationContactData data for the given user.
     * 
     * @param username The given username
     * @return A List of FullOrganizationContactData or empty
     */
    public List<FullOrganizationContactData> getFullOrgContactByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching FullOrganizationContactData for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return orgAPI.getFullOrgContacts(username);
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No OrganizationContact results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No OrganizationContact results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for any non-expired document records signed by the given user.
     * 
     * @param username The given username
     * @return A List of PeopleDocumentData or empty
     */
    public List<PeopleDocumentData> getPeopleDocumentsByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching PeopleDocuments for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return peopleAPI.getDocuments(username, true, false).stream().toList();
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No PeopleDocument results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No PeopleDocument results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for any non-expired document records signed by the given org.
     * 
     * @param orgId The given orgId
     * @return A List of OrganizationDocumentData or empty
     */
    public List<OrganizationDocumentData> getDocumentsByOrg(String orgId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching OrganizationDocuments for org: {}", TransformationHelper.formatLog(orgId));
        }
        try {
            return orgAPI.getOrganizationDocuments(orgId, true, false).stream().toList();
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No OrganizationDocument results for org: {}", TransformationHelper.formatLog(orgId), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No OrganizationDocument results for org: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(orgId), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for any PeopleEmailsData data for the given user.
     * 
     * @param username The given username
     * @return A List of PeopleEmailsData or empty
     */
    public List<Email> getEmailsByUsername(String username) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching PeopleEmailsData for user: {}", TransformationHelper.formatLog(username));
        }
        try {
            return peopleAPI
                    .getEmails(username)
                    .stream()
                    .map(e -> EfUserEmailBuilder.builder().id(e.emailID().toString()).mail(e.email()).build())
                    .sorted((e1, e2) -> e1.id().compareTo(e2.id()))
                    .toList();
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No PeopleEmailsData results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No PeopleEmailsData results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries Fdndb-api for any PeopleEmailsData data for the given user email that may be historic.
     * 
     * @param email The given email to check
     * @return A List of PeopleEmailsData or empty if none can be found
     */
    public Optional<String> getUsernameByEmail(String email) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching PeopleEmailsData for email: {}", TransformationHelper.formatLog(email));
        }
        try {
            List<PeopleEmailsData> emailData = peopleAPI.getEmailsByEmail(email);
            // check if we have more than 1 user, as that would make an indeterminate lookup and should be ignored
            Set<String> associatedUsers = emailData.stream().collect(Collectors.groupingBy(PeopleEmailsData::personID)).keySet();
            if (associatedUsers.size() > 1) {
                LOGGER
                        .warn("The email '{}' was associated with multiple accounts and cannot be used as a fallback: {}", email,
                                associatedUsers);
                return Optional.empty();
            }

            return emailData.stream().map(PeopleEmailsData::personID).findFirst();
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No PeopleEmailsData results for email: {}", TransformationHelper.formatLog(email), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No PeopleEmailsData results for email: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(email), e.getResponse().getStatus());
            }
        }
        return Optional.empty();
    }

    /**
     * Queries Fdndb-api for a People record for the given user. And extracts the alternate emails from the comments field.
     * 
     * @param username The given username
     * @return A List of alternate email strings or empty
     */
    public List<String> getAlternateEmails(String username, Optional<PeopleData> loadedPerson) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching People entity: {}", TransformationHelper.formatLog(username));
        }
        try {
            // Get comments field from People record using pre-loaded user if possible, alt emails are stored there
            String comments = loadedPerson.isPresent() ? loadedPerson.get().comments() : peopleAPI.getPerson(username).comments();
            if (StringUtils.isBlank(comments)) {
                return Collections.emptyList();
            }

            List<String> alternateEmails = new ArrayList<>();

            // Loop through to find any potential matches
            Matcher matcher = ALTERNATE_EMAIL_PATTERN.matcher(comments);
            while (matcher.find()) {
                // Add first capturing group, which is the email
                alternateEmails.add(matcher.group(1));
            }

            return alternateEmails;
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("FDNDB-API - No People results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("FDNDB-API - No People results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Queries the Gerrit API for any changes made by a given user ignoring the first 'x' changes, where x is the current number of tracked
     * changes.
     * 
     * @param email The given user's email
     * @param start The starting cursor position
     * @return A List of GerritChangeData or empty
     */
    public List<GerritChangeData> getGerritChanges(String username, int start) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching gerrit changes for user: {}", username);
        }

        try {
            return getAllGerritChanges(username, start);
        } catch (WebApplicationException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("GERRIT-API - No results for user: {}", TransformationHelper.formatLog(username), e);
            } else {
                LOGGER
                        .warn("GERRIT-API - No results for user: {}, code {}. Enable DEBUG logging for more detailed information",
                                TransformationHelper.formatLog(username), e.getResponse().getStatus());
            }
            return Collections.emptyList();
        }
    }

    /**
     * Fetches all Gerrit changes starting at previous count. Checks for the '_more_changes' field in the last record. If this field is set,
     * it will fetch the following page of results. It will fetch until there are no new changes.
     * 
     * @param query The search query
     * @param start The starting cursor position for fetching results.
     * @return A list of all the newest changes since the last query.
     */
    private List<GerritChangeData> getAllGerritChanges(String username, int start) {

        String query = "reviewer:" + username + " status:merged";

        List<GerritChangeData> out = new ArrayList<>();

        // Loop through all results until we hit the end flag
        while (true) {
            List<GerritChangeData> results = convertGerritJson(gerritAPI.getGerritChanges(query, start));
            out.addAll(results);

            // If '_more_changes' field exists in last record and is true, there is more
            if (results.isEmpty() || results.get(results.size() - 1).getMoreChanges() == null
                    || Boolean.FALSE.equals(results.get(results.size() - 1).getMoreChanges())) {
                break;
            }

            // increase the start point before getting next set
            start += results.size();
        }

        return out;
    }

    /**
     * Gerrit adds a special set of characters at the start of their response body. To be able to process the json properly, it must first
     * be removed. ex: )]}' [ ... valid JSON ... ]
     * 
     * @param body The Gerrit json body
     * @return A converted list of GerritchangeData entities if they exist
     */
    private List<GerritChangeData> convertGerritJson(String body) {
        try {
            // Parse the json body after the first 4 characters
            return objectMapper.readerForListOf(GerritChangeData.class).readValue(body.substring(GERRIT_RESPONSE_START_INDEX));
        } catch (Exception e) {
            LOGGER.error("Error parsing json Gerrit data", e);
            return Collections.emptyList();
        }
    }

    /**
     * When service is down and endpoint isn't found, a 404 is returned. This 404 will have different contents than if the user is simply
     * missing, so we can check for expected outputs. Will check if the status is 404, and if the body matches the expected output for when
     * a user is missing from the accounts API. Will return false otherwise
     * 
     * Fix for #110, #113
     * 
     * @param r the response to check for service down
     * @return true if the service appears to be down (unexpected 404 response), or else false
     */
    private boolean check404ForUserMissing(Response r) {
        // check that it's a 404, as this is a specific use case
        if (r.getStatus() != Status.NOT_FOUND.getStatusCode()) {
            return false;
        }

        String errorMessage = "";
        try (InputStream in = ((ResponseImpl) r).getEntityStream();
                InputStreamReader reader = new InputStreamReader(in);
                BufferedReader buff = new BufferedReader(reader)) {
            // needs to be read in first, as you can't reuse an input stream
            String rawJson = buff.lines().collect(Collectors.joining("\n"));
            LOGGER.trace("Checking 404 body for matched error format: {}", rawJson);
            // check object-based error message format
            errorMessage = attemptObjectBasedErrorReading(rawJson);

            // check legacy format
            if (StringUtils.isBlank(errorMessage)) {
                List<String> listEntity = objectMapper.readerForListOf(String.class).readValue(rawJson);
                if (listEntity.size() == 1) {
                    errorMessage = listEntity.get(0);
                }
            }
        } catch (Exception e) {
            // we want to swallow this exception in most cases, as it isn't useful and is noisy
            LOGGER.trace("Could not read value as legacy format", e);
        }

        return USER_NOT_FOUND_MESSAGE.equalsIgnoreCase(errorMessage);
    }

    private String attemptObjectBasedErrorReading(String in) {
        try {
            Map<String, String> error = objectMapper.readerFor(Map.class).readValue(in);
            if (error != null) {
                return error.get("message");
            }
        } catch (Exception e) {
            // we want to swallow this exception in most cases, as it isn't useful
            LOGGER.trace("Could not read value as modern format", e);
        }
        return null;
    }

    /**
     * Builds a bearer token using the token generated by the Oauth server. Will be removed once the accounts-api contains a way to get
     * account data via uid without auithentication.
     * 
     * @return A fully formed Bearer token
     */
    private String getBearerToken() {
        return "Bearer " + tokenService.getToken();
    }
}
