/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.models;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Outgoing response for the slack /u command
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_SlackResponse.Builder.class)
public abstract class SlackResponse {

    public abstract String getResponseType();

    public abstract String getText();

    public abstract List<SlackAttachment> getAttachments();

    public static Builder builder() {
        return new AutoValue_SlackResponse.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setResponseType(String responseType);

        public abstract Builder setText(String text);

        public abstract Builder setAttachments(List<SlackAttachment> attachments);

        public abstract SlackResponse build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_SlackResponse_SlackAttachment.Builder.class)
    public abstract static class SlackAttachment {

        public abstract String getTitle();

        public abstract String getTitleLink();

        public abstract String getText();

        public static Builder builder() {
            return new AutoValue_SlackResponse_SlackAttachment.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {

            public abstract Builder setTitle(String title);

            public abstract Builder setTitleLink(String titleLink);

            public abstract Builder setText(String text);

            public abstract SlackAttachment build();
        }
    }
}