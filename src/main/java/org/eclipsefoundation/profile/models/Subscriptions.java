/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.models;

import java.util.List;

import org.eclipsefoundation.profile.api.models.MailingListData;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * The return object for the mailing-lists endpoint.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_Subscriptions.Builder.class)
public abstract class Subscriptions {
    public abstract List<MailingListData> getMailingListSubscriptions();

    public static Builder builder() {
        return new AutoValue_Subscriptions.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setMailingListSubscriptions(List<MailingListData> subs);

        public abstract Subscriptions build();
    }
}
