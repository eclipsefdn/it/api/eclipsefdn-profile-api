/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.namespace;

/**
 * Class used to access the various LDAP fields used by the application.
 */
public final class LdapFieldNames {

    public static final String EMPLOYEE_TYPE = "employeeType";
    public static final String SN = "sn";
    public static final String CN = "cn";
    public static final String GIVEN_NAME = "givenName";

    private LdapFieldNames() {
    }
}
