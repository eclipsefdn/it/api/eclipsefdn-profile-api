/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.daos;

import org.eclipsefoundation.persistence.dao.impl.BaseHibernateDao;

import jakarta.inject.Named;
import jakarta.inject.Singleton;
import jakarta.persistence.EntityManager;

/**
 * DAO bound to the 'eclipse' DB. The default DAO for this application is bound
 * to the 'eclipse_api' DB.
 */
@Singleton
public class EclipsePersistenceDao extends BaseHibernateDao {

    private final EntityManager em;

    public EclipsePersistenceDao(@Named("eclipse") EntityManager manager) {
        this.em = manager;
    }

    @Override
    protected EntityManager getPrimaryEntityManager() {
        return em;
    }
}
