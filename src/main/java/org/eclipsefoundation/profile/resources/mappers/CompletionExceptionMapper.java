/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources.mappers;

import java.util.Arrays;
import java.util.concurrent.CompletionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.Priority;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * Custom exception mapper class used to map 500 responses when there is an error fetching data asynchronously.
 */
@Provider
@Priority(100)
public class CompletionExceptionMapper implements ExceptionMapper<CompletionException> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CompletionExceptionMapper.class);

    @Override
    public Response toResponse(CompletionException exception) {
        LOGGER.error(exception.getMessage(), exception);
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity(Arrays.asList("Error while fetching profile data, profile unavailable.")).build();
    }
}