/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.profile.resources;

import java.util.Optional;

import org.eclipsefoundation.caching.model.CacheWrapper;
import org.eclipsefoundation.caching.model.ParameterizedCacheKeyBuilder;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.http.annotations.AuthenticatedAlternate;
import org.eclipsefoundation.http.exception.ApplicationException;
import org.eclipsefoundation.profile.namespace.ProfileAPIParameterNames;
import org.eclipsefoundation.profile.services.ProfileService;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.jboss.resteasy.reactive.NoCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

/**
 * Resource class for all profile fetches related to en EF user's Github handle.
 */
@NoCache
@Path("github/profile")
@Produces({ MediaType.APPLICATION_JSON })
public class GithubResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(GithubResource.class);

    private static final String USER_NOT_FOUND_MSG = "User not found.";

    private final ProfileService profileService;
    private final CachingService cache;

    public GithubResource(ProfileService profileService, CachingService cache) {
        this.profileService = profileService;
        this.cache = cache;
    }

    /**
     * Returns a 200 OK Response containing the requested EfUser entity. If the request is unauthenticated, the email and country fields
     * will me anonymized. Returns a 404 Not Found Response if the user cannot be found. Returns a 500 if there is an issue with the LDAP or
     * Accounts-API connections.
     * 
     * @param handle The given Github handle.
     * @return A 200 OK Response containing the requested EfUser entity. Returns a 404 Not Found Response if the user cannot be found.
     */
    @GET
    @AuthenticatedAlternate(allowPartialResponse = true)
    @Path("{handle}")
    public Response getUserProfileByHandle(@PathParam("handle") String handle) {
        // Cache result using "handle" strategy
        MultivaluedMap<String, String> cacheParams = new MultivaluedHashMap<>();
        cacheParams.add(ProfileAPIParameterNames.STRATEGY.getName(), "github");
        CacheWrapper<EfUser> cacheResult = cache.get(handle, cacheParams, EfUser.class, () -> profileService.getProfileByHandle(handle));

        // ApplicationException is thrown when LDAP or accounts fails. Clear cache and return 500
        Optional<Class<?>> errorType = cacheResult.errorType();
        if (errorType.isPresent() && errorType.get().equals(ApplicationException.class)) {
            cache.remove(ParameterizedCacheKeyBuilder.builder().clazz(EfUser.class).id(handle).params(cacheParams).build());
            throw new ApplicationException("Failed connection to internal service");
        }

        // No ApplicationException means the user just doesn't exist in LDAP or accounts
        Optional<EfUser> profile = cacheResult.data();
        if (profile.isEmpty()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Unable to retrieve profile data for GH handle: {}", TransformationHelper.formatLog(handle));
            }

            throw new NotFoundException(USER_NOT_FOUND_MSG);
        }

        return Response.ok(profile.get()).build();
    }
}
